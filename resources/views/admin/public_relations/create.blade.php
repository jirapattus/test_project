@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="card text-center">
        <div class="card-header">
            <h2>เพิ่มประกาศ</h2>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label for="Main_figure" class="col-sm-2 col-form-label">หัวเรื่อง</label>
                <div class="col-sm-10">
                    <input id="Main_figure" type="file" name="myImage" class=" form-control-user"
                        accept="image/x-png,image/gif,image/jpeg" />
                </div>
            </div>
            <div class="form-group row">
                <label for="title" class="col-sm-2 col-form-label">หัวเรื่อง</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="title">
                </div>
            </div>
            <div class="form-group row">
                <label for="content" class="col-sm-2 col-form-label">เนื้อหา</label>
                <div class="col-sm-10">
                    <textarea name="descriptions" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">รายละเอียด</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="staticEmail">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">หัวเรื่อง</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="staticEmail">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">หัวเรื่อง</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="staticEmail">
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">หัวเรื่อง</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="staticEmail">
                </div>
            </div>
        </div>
        <div class="card-footer text-muted">
            2 days ago
        </div>
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('descriptions');

</script>
@endsection
