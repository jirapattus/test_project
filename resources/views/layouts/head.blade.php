<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ระบบกิจกรรมนักศึกษา ชมรมดนตรีนาฏศิลป์พื้นบ้าน มหาวิทยาลัยราชภัฏสกลนคร</title>

    <link rel="icon" href="{{asset('assets/general/img/icon/icon.png')}}" type="image/x-icon">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('assets/general/css/sb-admin-2.min.css') }}" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/general/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/general/vendor/animate.css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/general/vendor/icofont/icofont.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/general/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/general/vendor/venobox/venobox.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/general/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/general/vendor/aos/aos.css') }}" rel="stylesheet">
    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/general/css/style.css') }}" rel="stylesheet">

    @stack('stylesheet')
</head>
