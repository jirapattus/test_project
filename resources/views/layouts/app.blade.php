<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.head')

<body style="background-color: #669E9A;">
    <div id="app">
            @include('layouts.header')
        <main class="py-2" >
            @yield('content')
        </main>
    </div>
</body>
@include('layouts.script')

</html>
