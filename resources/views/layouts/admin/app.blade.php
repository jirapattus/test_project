<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.admin.head')

<body id="page-top" style="background-color: #669E9A;">

    <!-- Page Wrapper -->
    <div id="wrapper">
        @include('layouts.admin.left_menu')
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                @include('layouts.admin.header')
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>



        </div>
    </div>
</body>
@include('layouts.admin.script')

</html>
