<!-- Page level plugins -->
<script type="text/javascript" src="{{ asset('assets/admin/vendor/chart.js/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/vendor/chart.js/Chart.bundle.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/vendor/chart.js/Chart.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/vendor/chart.js/Chart.js') }}"></script>

<!-- Bootstrap core JavaScript-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript" src="{{ asset('assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Core plugin JavaScript-->
<script type="text/javascript" src="{{ asset('assets/admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- Custom scripts for all pages-->
<script type="text/javascript" src="{{ asset('assets/admin/js/sb-admin-2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@stack('script')
