        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top header-transparent">
            <div class="container">

                <div class="logo float-left">
                    <!-- <h1 class="text-light"><a href="index.html"><span>Moderna</span></a></h1> -->
                    <!-- Uncomment below if you prefer to use an image logo -->
                    <a href="{{ url('/') }}"><img src="assets/general/img/icon/icon.png" alt="" class="img-fluid"></a>
                </div>

                <nav class="nav-menu float-right d-none d-lg-block">
                    <ul>
                        <li class="active"><a href="{{url('/')}}">หน้าแรก</a></li>
                        <li class="drop-down"><a href="">ขอความอนุเคราะห์การแสดง</a>
                            <ul>
                                <li><a href="#">ติดต่อการแสดง</a></li>
                                <li><a href="#">สถานะการติดต่อ</a></li>
                                <li><a href="#">อื่นๆ</a></li>
                            </ul>
                        </li>
                        <li><a href="services.html">กิจกรรม</a></li>
                        <li><a href="{{ url('public_relations') }}">ประกาศ</a></li>
                        <li><a href="{{ url('/contact')}}">ติดต่อ</a></li>
                        @guest
                        @else
                        <li class="drop-down">
                            <a href=""><img src="{{ asset('/storage/uploads/images/avatars/'.Auth::user()->myImage) }}"
                                    class="radius" width="33px" height="33px" /></a>
                            <ul>
                                <li><a href="#">
                                        <center><img src="{{ asset('/storage/uploads/images/avatars/'.Auth::user()->myImage) }}"
                                                width="100px" height="100px" /></center>
                                    </a></li>
                                @if(auth()->user()->isadmin())
                                <li><a href="{{route('admin.home')}}">จัดการระบบ</a></li>
                                @endif
                                <li><a href="#">แก้ไขข้อมูลส่วนตัว</a></li>
                                <li> <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                                        <i class="material-icons"></i> {{ __('ล็อกเอาท์') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                    </ul>
                </nav><!-- .nav-menu -->
            </div>
        </header><!-- End Header -->

