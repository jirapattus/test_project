    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}" defer></script>

    <!-- Vendor JS Files -->
    <script type="text/javascript" src="{{ asset('assets/general/vendor/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/php-email-form/validate.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/venobox/venobox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/counterup/counterup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/general/vendor/aos/aos.js') }}"></script>

    <!-- Template Main JS File -->
    <script type="text/javascript" src="{{ asset('assets/general/js/main.js') }}"></script>

      <!-- Custom scripts for all pages-->
    <script type="text/javascript" src="{{ asset('assets/general/js/sb-admin-2.min.js') }}"></script>
    @stack('script')
