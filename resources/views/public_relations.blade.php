@extends('layouts.app')
<main id="main">

    <!-- ======= Contact Section ======= -->
    <section class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>ประกาศ</h2>
                <ol>
                    @guest
                    <li><a href="{{ route('login') }}">{{ __('เข้าสู่ระบบ') }}</a> </li>
                    @if (Route::has('register'))
                    <li><a href="{{ route('register') }}">{{ __('สมัครสมาชิก') }}</a></li>
                    @endif
                    @else
                    <li><i class="icofont-user-alt-3"></i> {{ Auth::user()->fname . " " . Auth::user()->lname }}
                        <br>
                        @if(Auth::user()->role == 1)
                        <small> [แอดมิน] </small>

                        @elseif(Auth::user()->role == 0)
                        <small> [สมาชิก] </small>

                        @elseif(Auth::user()->role == 2)
                        <small> [ดูสรุป] </small>

                        @endif
                    </li>
                    @endguest
                </ol>
            </div>

        </div>
    </section><!-- End Contact Section -->
</main>
@section('content')
    <public-reations></public-reations>
@endsection
