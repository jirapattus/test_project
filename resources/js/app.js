require('./bootstrap');

window.Vue = require('vue');
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('user', require('./components/User.vue').default);
Vue.component('contact', require('./components/general/a_contact.vue').default);
Vue.component('public-reations', require('./components/general/public_reations.vue').default);
const app = new Vue({
    el: '#app',
});
