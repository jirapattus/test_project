<?php

use Illuminate\Database\Seeder;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'nametitle' => 'นาย',
            'fname' => 'jirapatadmin',
            'lname' => 'test',
            'email' => 'test1@test.com',
            'password' => Hash::make('123456789'),
            'role' => '1',
            'tel' => '0888888888',
            'myImage' => 'user_default.png',
            ],
            [
            'nametitle' => 'นาย',
            'fname' => 'jirapatuser',
            'lname' => 'test',
            'email' => 'test2@test.com',
            'password' => Hash::make('123456789'),
            'role' => '0',
            'tel' => '0888888888',
            'myImage' => 'user_default.png',
            ],
            [
            'nametitle' => 'นาย',
            'fname' => 'jirapatboss',
            'lname' => 'test',
            'email' => 'test3@test.com',
            'password' => Hash::make('123456789'),
            'role' => '2',
            'tel' => '0888888888',
            'myImage' => 'user_default.png',
            ],
        ]);
    }
}
