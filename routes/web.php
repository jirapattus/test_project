<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); });
Route::get('/contact', function () { return view('contact'); });
Route::get('/public_relations', function () { return view('public_relations'); });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');
Route::get('admin/public_relations', 'Public_relationsController@index')->name('admin.public_relations')->middleware('is_admin');
Route::get('admin/public_relations/create', 'Public_relationsController@create')->name('admin.public_relations.create')->middleware('is_admin');
Route::resource('users', 'UserController');

